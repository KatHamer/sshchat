# SSHChat

A cursed chat application built on top of SSH.

## Getting Started

- Install lxc and lxd
- Follow [this guide](https://linuxcontainers.org/lxc/getting-started/) to get lxc and lxd set up, make sure you are able to create and launch unprivellaged containers before proceeding
- Run `init.sh` on your host machine (Still a work in progress) to automatically create and setup the sshchat container
- Run `$ lxc project switch sshchat` to make sure you have the sshchat project selected in lxc
- Attach to the container's console by running `$ lxc console sshchat`
- You will be asked to login to the container, use the username `sshchat` and the password you chose during setup
- CD into `~/.sshchat` and activate the sshchat venv by running `$ source venv/bin/activate`
- Run `python __main__.py`

## Connecting to the server

- Find out the container's IPV4 address by running `$ lxc ls`, this will show a list of containers in the current project
- Connect to the SSHChat server by running ```$ ssh <username>@<container-ip> -p <server_port>```, username can be whatever you would like your chat nickname to be, container-ip is the IP we just copied from the `lxc ls` command, and server port is by default 2200
- You will be greeted with an ascii-art banner and some text, you can type in chat messages and they will be echoed back to you, nothing else works at the moment.
- Press CTRL-D to close the connection.

## How it works

This is essentially a Paramiko SSH server with dummy auth, in the `Server.check_auth_publickey` function which would usually be used to validate a client's public key, we simply return `paramiko.AUTH_SUCCESSFUL`, this allows us to bypass authentication entirely and login to the server with an arbitrary username.

## Future plans

The next thing I plan to implement is threading, and a proper asynchronous send/receive message system, after that I would like to develop a nickname registration system, and a group chat feature.