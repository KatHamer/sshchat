#!/bin/bash
# init.sh - create container and set it up

# NOTE/WARNING - This script is super rough around the edges
set -e

init() {( set -e
    echo "Creating container..."
    lxc project create sshchat -c features.images=false -c features.profiles=false
    lxc project switch sshchat
    lxc launch ubuntu:20.04 sshchat
    echo "Creating user..."
    lxc exec sshchat -- /bin/bash -c "useradd sshchat"
    echo "[Interaction needed] Enter password for sshchat user."
    lxc exec sshchat -- /bin/bash -c "passwd sshchat"
    lxc exec sshchat -- /bin/bash -c "cp -r /etc/skel/ /home/sshchat/"
    lxc exec sshchat -- /bin/bash -c "chown -R sshchat /home/sshchat"
    lxc exec sshchat -- /bin/bash -c "chsh sshchat --shell /bin/bash"
    echo "Updating package indexes..."
    lxc exec sshchat -- /bin/bash -c "apt update"
    echo "Installing dependencies..."
    lxc exec sshchat -- /bin/bash -c "apt install -y python3-pip python-is-python3 python3-venv libkrb5-dev git"
    echo "Copying files..."
    lxc exec sshchat -- /bin/bash -c "mkdir /home/sshchat/sshchat/"
    lxc file push __main__.py sshchat/home/sshchat/sshchat/
    lxc file push requirements.txt sshchat/home/sshchat/sshchat/
    lxc file push README.md sshchat/home/sshchat/sshchat/
    echo "Generating dummy RSA key..."
    lxc exec sshchat -- /bin/bash -c 'ssh-keygen -t rsa -N "" -f /home/sshchat/sshchat/test_rsa.key'
    lxc exec sshchat -- /bin/bash -c "chown -R sshchat /home/sshchat/sshchat/"
    echo "Creating venv..."
    lxc exec sshchat -- /bin/bash -c "cd /home/sshchat/sshchat;python3 -m venv venv"
    echo "Installing Python dependencies..."
    lxc exec sshchat -- /bin/bash -c "cd /home/sshchat/sshchat;source /home/sshchat/sshchat/venv/bin/activate;pip3 install -r requirements.txt"
)}

cleanup() {
    echo "Cleaning up..."
    lxc project switch sshchat
    lxc stop sshchat
    lxc delete sshchat
    lxc project delete sshchat
}

main() {
    init || {
        echo "Error occured during setup!"
        cleanup
        exit 1
    } 
    echo "Done! Run lxc console sshchat to attach to your server's console."
}

main $@
