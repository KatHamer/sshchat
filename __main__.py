#!/usr/bin/env python3

"""
SSHChat
A cursed chat application built on top of SSH
By Katelyn Hamer

Adapted from this Paramiko example file: https://github.com/paramiko/paramiko/blob/master/demos/demo_server.py
"""

import socket
import threading
import logging
import time
import paramiko
from paramiko.py3compat import b, u, decodebytes


# Setup logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.StreamHandler(),
    ]
)


def send_welcome_message(channel, username):
    """Send a wecome message apon connection to our server"""
    welcome_message = (
        r" ____ ____  _   _  ____ _           _",
        r"/ ___/ ___|| | | |/ ___| |__   __ _| |_",
        r"\___ \___ \| |_| | |   | '_ \ / _` | __|",
        r" ___) |__) |  _  | |___| | | | (_| | |_",
        r"|____/____/|_| |_|\____|_| |_|\__,_|\__|",
        "\n"
        f"Welcome to SSHChat, {username}!\n"
    )
    for line in welcome_message:
        channel.send(line+"\r\n")


class Server(paramiko.ServerInterface):
    def __init__(self):
        self.event = threading.Event()

    def check_channel_request(self, kind, chanid):
        if kind == "session":
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED

    def check_auth_publickey(self, username, key):
        """We accept any public key since our desired outcome is to allow no authentication with arbitrary usernames"""
        self.username = username  # Hacky storage of the supplied username
        return paramiko.AUTH_SUCCESSFUL

    def get_allowed_auths(self, username):
        return "publickey"

    def check_channel_shell_request(self, channel):
        self.event.set()
        return True

    def check_channel_pty_request(
        self, channel, term, width, height, pixelwidth, pixelheight, modes
    ):
        return True


def start_server(port=2200):
    logger.debug(f"Binding to port {port}...")
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(("", port))
    except Exception as exc:
        logger.error(f"Failed to bind on port {port}!")
        logger.exception(exc)
        exit(1)

    logger.info(f"Now listening for connections on port {port}...")
    try:
        sock.listen(100)
    except Exception as exc:
        logger.error(f"Failed to listen on socket!")
        exit(1)

    client, addr = sock.accept()
    logger.info("We have a connection!")

    try:
        transport = paramiko.Transport(client)
        host_key = paramiko.RSAKey(filename="test_rsa.key")
        transport.add_server_key(host_key)
        server = Server()

        try:
            logger.info("Starting server...")
            transport.start_server(server=server)
        except paramiko.SSHException as exc:
            logger.error("SSH negotiation failed!")
            logger.exception(exc)
            exit(1)

        # Do fake authentication
        channel = transport.accept(20)
        if channel is None:
            logger.error("No channel.")
            exit(1)
        logger.info(f"Authenticated {server.username}!")

        server.event.wait(10)
        if not server.event.is_set():
            logger.error("Client never asked for a shell.")

        send_welcome_message(channel, server.username)  # Send a greeting

        # I don't know what this does
        f = channel.makefile("rU")

        # Hacky prompt send receive thing
        prompt = f"[{server.username}]> "
        channel.send(prompt)
        while True:
            if channel.recv_ready():
                stream = ""
                while (byte := channel.recv(8)) !=  b'\r':
                    logger.debug(f"Byte from client: {byte}")
                    if byte == b'\x04':  # Ctrl-D: disconnect
                        channel.send("Goodbye!\r")
                        channel.close()
                        break
                    else:  # Anything else, append to our primative stream
                        stream += (byte.decode())
                        channel.send(str(byte.decode()))
                channel.send(f'\n\r{prompt}')
                logger.debug(f"Data stream from client: {stream}")
            time.sleep(0.1)

    except Exception as exc:
        logger.error("Exception occured in main server loop!")
        logger.exception(exc)
    finally:
        try:
            channel.close()
        except UnboundLocalError:  # If we haven't set up a channel yet
            pass


if __name__ == "__main__":
    logger.debug("Starting SSHChat server...")
    start_server()
